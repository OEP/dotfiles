CWD := $(shell pwd)

all: install
.PHONY: all

################################################################################
# shared functions
define symlink
	test -L $(2) || ln -sf $(1) $(2)
endef


################################################################################
# install
install:
	$(MAKE) install-tmux
	$(MAKE) install-bash
	$(MAKE) install-vim
	$(MAKE) install-python
	$(MAKE) install-git

################################################################################
# bash
install-bash:
	$(MAKE) install-bash-bashrcd
	$(call symlink, $(CWD)/bashrc.d, ~/.bashrc.d)
	$(call symlink, $(CWD)/inputrc, ~/.inputrc)

install-bash-bashrcd:
	touch ~/.bashrc
	grep -q bashrc.d ~/.bashrc || cat $(CWD)/bashrcd.bash >>~/.bashrc

################################################################################
# tmux
install-tmux:
	$(call symlink, $(CWD)/tmux.conf, ~/.tmux.conf)

################################################################################
# vim
install-vim:
	$(MAKE) install-vim-pathogen
	mkdir -p ~/.vim
	$(call symlink, $(CWD)/vim/vimrc, ~/.vimrc)
	$(call symlink, $(CWD)/vim/bundle, ~/.vim/bundle)

install-vim-pathogen:
	mkdir -p ~/.vim/autoload
	test -e ~/.vim/autoload/pathogen.vim || curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

################################################################################
# python
install-python:
	$(call symlink, $(CWD)/pythonrc.py, ~/.pythonrc.py)


################################################################################
# git
define git_alias
	git config --global $(1) >/dev/null || git config --global $(1) $(2)
endef

install-git:
	$(MAKE) install-git-prompt
	ln -sf $(CWD)/git/gitignore ~/.gitignore_global
	$(call git_alias, user.name, "Paul Kilgo")
	$(call git_alias, user.email, "paulkilgo@gmail.com")
	$(call git_alias, alias.ci, commit)
	$(call git_alias, alias.st, status)
	$(call git_alias, alias.co, checkout)
	$(call git_alias, alias.ff, "pull --ff-only")
	$(call git_alias, push.default, simple)
	$(call git_alias, core.excludesfile, ~/.gitignore_global)
	$(call git_alias, alias.lg, "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit")

install-git-prompt:
	test -f $(CWD)/bashrc.d/git-prompt.bash || wget "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh" -O $(CWD)/bashrc.d/git-prompt.bash
