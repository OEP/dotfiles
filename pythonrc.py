import atexit
import os
import sys

try:
    import readline
except ImportError:
    pass
else:
    import rlcompleter
    readline.parse_and_bind('tab:complete')

# History
historyPath = os.path.expanduser("~/.pyhistory")


def save_history(historyPath=historyPath):
    import readline
    readline.write_history_file(historyPath)

try:
    readline.read_history_file(historyPath)
except IOError:
    pass

atexit.register(save_history)

# anything not deleted (sys and os) will remain in the interpreter session
del atexit, readline, rlcompleter, save_history, historyPath
