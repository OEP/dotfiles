export EDITOR=vim

# Ctrl+W treats "/" as a word boundary
stty werase undef >/dev/null 2>&1
bind '\C-w:unix-filename-rubout' >/dev/null 2>&1

shopt -s autocd
shopt -s dirspell
shopt -s failglob
shopt -s globstar
shopt -s histappend

# git-prompt.sh PS1
PROMPT_COMMAND='__git_ps1 "\n\t [\e[1;34m\u@\h:\e[1;33m\w\e[0m]" "\n\\\$ "'
