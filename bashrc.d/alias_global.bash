alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias ll='ls -lah --color=auto'

alias dk=docker
alias dkc=docker-compose
alias agent='eval $(ssh-agent); ssh-add'

alias gg='git grep'

# goofs
alias gerp=grep
alias grpe=grep
alias gi=git
alias got=git
alias gti=git
